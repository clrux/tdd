var chai = require('chai'),
    expect = chai.expect,
    assert = chai.assert,
    should = chai.should();

describe ('Test example', function() {
    this.timeout(10000);

    describe ('Visit Google to search', function() {

        it ('can navigate to the Google search homepage', function() {
            browser.url('https://www.google.com');
            expect(browser.getTitle()).to.be.string('Google');
        });

        it ('can find the search box', function() {
            browser.isVisible('#lst-ib', function(searchBox) {
                expect(searchBox).to.be.true;
            });
        });
    });

    describe ('Perform a search', function() {

        it ('can navigate to Google Search', function() {
            browser.url('https://www.google.com');
        });

        it ('can perform a search', function() {
            browser.setValue('#lst-ib', 'Deque accessibility');
            browser.pause(2000);
            browser.getTitle(function(title) {
                expect(title).to.have.string('Deque accessibility - Google Search');
            });
        });
    });
});