#!/usr/bin/env node

/*
 * This isn't quite working yet.
 * I'd like for an easy approach to starting the
 * Selenium server without having to locate the
 * JAR file that comes with this package.
 */
var exec = require('child_process').exec;

exec('java -jar ../selenium/selenium-server-standalone-3.0.0-beta4.jar -role node', function(error, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);

    if (error !== null) {
        console.log('Could not start Selenium server. See https://bitbucket.org/downzero/tdd#readme for more information.');
    }
});