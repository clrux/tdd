# Amaze test-driven development

This is a set of tools that will allow for test-driven development using the following technologies:

* Mocha (test runner)
* Chai (an assertion library)
* webdriverio (browser control bindings)
* Selenium (browser abstraction and factory)
* PhantomJS (headless browser)

## Install

`npm install -g amaze-tdd`

## Running tests

**First, start the Selenium server with:**

`java -jar selenium/selenium-server-standalone-3.0.0-beta4.jar`

**Then run tests with:**

`amaze test`

## Dependencies

You will also need to add the webdriver locations to your `$PATH`. They come in this package, but you will want to move them somewhere static. Then point that location using:

`export PATH=$PATH:location/to/webdriver/folder`

## Test files

Our process will run Mocha on any files in the specified path with the format `*-spec.js`. This means that the following examples will work:

* `test-spec.js`
* `overlay-name-spec.js`
* `camelCase-spec.js`
* `ALLCAPS-spec.js`
* `some-folder/test-spec.js`
* `some-folder/nested-folder/test-spec.js`

Basically any file that ends with `-spec.js` will be covered in testing.

## Troubleshooting

If you encounter errors, this section may be able to help.

### "Could not start Selenium server..."

The Node application can't automatically start your Selenium server so you'll need to start it manually. You can do so by finding the `.jar` file in `node_modules/amaze-tdd/selenium` or similar, and running:

`java -jar path/to/selenium/selenium-server-standalone-3.0.0-beta4.jar`

You should run this in a separate Terminal instance.
