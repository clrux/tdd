#!/usr/bin/env node

var net = require('net'),
    server = net.createServer(),
    child = require('child_process'),
    spawn = child.spawn,
    exec = child.exec;

console.log('');
console.log('>> AMAZE FUNCTIONAL TESTS <<');
console.log('');
console.log('... Checking if Selenium server is running...');

server.once('error', function(err) {
    if (err.code === 'EADDRINUSE') {
        return true;
    } else {
        return false;
    }
});

server.once('listening', function() {
    server.close();
});

if (server.listen(4444)) {
    console.log('... Good!');
    console.log('... Running tests...');
    console.log('');

    exec('node ' + __dirname + '/node_modules/webdriverio/bin/wdio ' + __dirname + '/wdio.conf.js', function(error, stdout, stderr) {
        console.log(stdout, stderr);
        if (error !== null) {
            console.log(error);
        }
    });
    return true;
} else {
    console.log('... Failed! See https://bitbucket.org/downzero/tdd#readme for more information.');
    console.log('');
    return false;
}