exports.config = {
    port: 4444,
    specs: [
        './**/*-spec.js'
    ],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 1,
    capabilities: [{
        browserName: 'firefox'  // firefox, chrome, phantomjs
    }],
    sync: true,
    logLevel: 'silent',  // silent, verbose, command, data, result, error
    coloredLogs: true,
    screenshotPath: './screenshots/',
    baseUrl: 'http://localhost:3003',
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd'
    }
};
